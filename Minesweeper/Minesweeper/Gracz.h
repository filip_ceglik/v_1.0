#pragma once

#include "stdafx.h"
#include "Plansza.h"

using namespace std;

class Gracz
{
public:
	string imie;
	int wynik;
	int pkt_x, pkt_y;
	Gracz(string name = "Douchebag", int score = 0, int x_axis = NULL, int y_axis = NULL);
	int zwieksz_wynik(Gracz player);
	void postaw_punkt(Mina(*tab)[10], Gracz player);


};