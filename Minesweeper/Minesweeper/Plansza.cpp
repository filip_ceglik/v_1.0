#include "stdafx.h"
#include <iostream>
#include "Plansza.h"
#include <Windows.h>

using namespace std;



void Plansza::stworz(Mina(*tab)[10],Mina *tablica_min, Mina obiekt) 
{
	for (int i = 0; i <= 9; i++)
	{
		for (int j = 0; j <= 9; j++)
		{
			if (i == tablica_min[i].x || j == tablica_min[i].y)
			{
				tab[i][j].wyglad = tablica_min[i].wyglad;
				tab[i][j].wartosc = tablica_min[i].wartosc;
			}
			else
			{
				tab[i][j].wyglad = '1';
			}
		}
	}
}

void Plansza::losuj_wspolrzedne(Mina *tablica_min)
{
	for (int i = 0; i < 10; i++)
	{
			tablica_min[i].x = (rand() % 9) + 0; //cos tam dziala
			tablica_min[i].y = (rand() % 10) + 0;
			tablica_min[i].widocznosc = false;
	}
}


void Plansza::wyswietl(Mina(*tab)[10], Mina obiekt)
{
	cout << endl << endl;
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			if (tab[i][j].widocznosc == true)
				if (tab[i][j].wartosc == 0)
					tab[i][j].wyglad = ' ';
				else
				{
					tab[i][j].wyglad = tab[i][j].wartosc;
				}
			if (tab[i][j].widocznosc == false)
				tab[i][j].wyglad = '#';
			cout << tab[i][j].wyglad; //wyswietlic 
			//cout << tab[i][0].wyglad;
		}
		cout << "\n";
	
	}
}

void Plansza::odkryj(int x, int y, Gracz player, Mina (*tab)[10])
{
	if (player.pkt_x < 0 || player.pkt_y > 9) return;
	if (player.pkt_y < 0 || player.pkt_y > 9) return;
	if (tab[player.pkt_x][player.pkt_y].widocznosc == true) return;
	if (tab[player.pkt_x][player.pkt_y].wartosc != 9 && tab[player.pkt_x][player.pkt_y].widocznosc == false)
		tab[player.pkt_x][player.pkt_y].widocznosc = true;

	if (tab[player.pkt_x][player.pkt_y].wartosc > 0) return;

	odkryj(tab[x][y].x - 1, tab[x][y].y-1, player, tab);
	odkryj(tab[x][y].x - 1, tab[x][y].y, player, tab);
	odkryj(tab[x][y].x - 1, tab[x][y].y + 1, player, tab);
	odkryj(tab[x][y].x + 1, tab[x][y].y - 1, player, tab);
	odkryj(tab[x][y].x + 1, tab[x][y].y, player, tab);
	odkryj(tab[x][y].x + 1, tab[x][y].y + 1, player, tab);
	odkryj(tab[x][y].x, tab[x][y].y - 1, player, tab);
	odkryj(tab[x][y].x, tab[x][y].y, player, tab);
	odkryj(tab[x][y].x, tab[x][y].y + 1, player, tab);
}

void Plansza::sterowanie(Mina(*tab)[10], Gracz &player)
{
	int x, y, koniec, x1, y1;
	if ((GetKeyState(enter) & 0x8000))
	{
		if (tab[x][y].wartosc == 9) //trafiles na mine
			koniec = 2;

		odkryj(tab[x][y].x, tab[x][y].y, player, tab); //odkrywanie p�l
		wyswietl(tab, tab[x][y]); // wyswietl plansze
	}

	if ((GetKeyState(strzalka_prawo) & 0x8000) && x<9) x++;
	if ((GetKeyState(strzalka_lewo) & 0x8000) && x>0) x--;
	if ((GetKeyState(strzalka_dol) & 0x8000) && y<9) y++;
	if ((GetKeyState(strzalka_gora) & 0x8000) && y>0) y--;

	if (y1 == y && x1 == x) return; //je�eli nie ma ruchu wyjdz


	wyswietl(tab, tab[x][y]); // wyswietl plansze
}