#pragma once

#include <iostream>
#include <vector>
#include "conio.h"
#include<stdlib.h>
#include <cstdlib>
#include<time.h>
#include "Mina.h"
#include <ctime>
#include "Minesweeper.h"

using namespace std;

class Plansza
{
	friend class Gracz;
	friend class Mina;
public:
	Plansza() = default;
	Mina pole[10][10];
	void stworz(Mina(*tab)[10], Mina *tablica_min, Mina obiekt); //utworzenie planszy | obecnie plansza[9][10]
	void losuj_wspolrzedne(Mina *tablica_min);
	void wyswietl(Mina(*tab)[10], Mina obiekt);
	void odkryj(int x, int y, Gracz player, Mina(*tab)[10]);
	void sterowanie(Mina(*tab)[10], Gracz &player);
};